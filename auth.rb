require 'google/apis/compute_v1'

compute = Google::Apis::ComputeV1::ComputeService.new
compute.authorization = Google::Auth::ServiceAccountCredentials.from_env(['https://www.googleapis.com/auth/compute.readonly'])

aggregated_instances = compute.list_aggregated_instances(ENV['GOOGLE_CLOUD_PROJECT_NAME'])
aggregated_instances.items.each do |zone, instance_list|
  next unless instance_list.instances
  instance_list.instances.each do |instance|
    external_ip = instance.network_interfaces.map do |network_interface|
      network_interface.access_configs.
        select { |c| c.name.downcase == 'external nat'}.
        map    { |c| c.nat_ip }
    end.flatten.first

    puts "#{zone} #{external_ip} #{instance.name}"
  end
end
