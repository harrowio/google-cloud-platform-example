require 'sshkit'
require 'sshkit/dsl'
include SSHKit::DSL

SSHKit::Backend::Netssh.configure do |ssh|
  ssh.ssh_options = {
    user: 'root',
    verbose: :debug,
    auth_methods: ['publickey'],
  }
end

on '104.155.14.255' do
  puts capture(:hostname)
end
